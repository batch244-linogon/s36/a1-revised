// controllers - business logic
const Task = require("../models/task");

// controller for getting all tasks
module.exports.getAllTasks = () => {
	
	return Task.find({}).then(result => {
		return result;
	});
};

// creating tasks
module.exports.createTask = (requestBody) => {
	
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// deleting tasks
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

//get task
// Activity - get a specific task

module.exports.getSpecificTask = (taskId) =>{
	return Task.findById(taskId).then(result => {
		return result;
	})
}
// updating task
// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} else {

			console.log(result)
			result.name = newContent.name;

			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false;
				} else {
					return updatedTask;
				}
			})
		}
	})
}

